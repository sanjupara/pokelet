import React, { useState, useEffect } from 'react';
import { View, ActivityIndicator, Alert, StyleSheet } from 'react-native';
import { Accelerometer } from 'expo-sensors';
import { Audio } from 'expo-av';
import { getPokemon, getPokemonCryUrl } from './services/pokemonService';
import PokemonImage from './components/PokemonImage';
import PokemonInfo from './components/PokemonInfo';
import NoShinyText from './components/NoShinyText';

export default function Main() {
  const [pokemon, setPokemon] = useState(null);
  const [loading, setLoading] = useState(true);
  const [shiny, setShiny] = useState(false);
  const [noShinyText, setNoShinyText] = useState("");

  useEffect(() => {
    loadPokemon();

    Accelerometer.setUpdateInterval(500);

    const subscription = Accelerometer.addListener(({ x, y, z }) => {
      if (Math.abs(x) > 1.5 || Math.abs(y) > 1.5 || Math.abs(z) > 1.5) {
        loadPokemon();
      }
    });

    return () => subscription && subscription.remove();
  }, []);

  const playSound = async (url) => {
    try {
      const { sound } = await Audio.Sound.createAsync({ uri: url });
      await sound.playAsync();
    } catch (error) {
      Alert.alert('Fehler', 'Es gab ein Problem beim Abspielen des Sounds.');
    }
  };

  const loadPokemon = async () => {
    setLoading(true);
    setShiny(false);
    setNoShinyText("");
    try {
      const randomId = Math.floor(Math.random() * 898) + 1;
      const loadedPokemon = await getPokemon(randomId);
      setPokemon(loadedPokemon);

      const cryUrl = getPokemonCryUrl(randomId);
      playSound(cryUrl);
    } catch (error) {
      Alert.alert('Fehler', 'Es gab ein Problem beim Laden des Pokémon.');
    } finally {
      setLoading(false);
    }
  };

  const handlePress = async () => {
    if (pokemon) {
      const cryUrl = getPokemonCryUrl(pokemon.id);
      playSound(cryUrl);

      if (pokemon.sprites.front_shiny) {
        setShiny(!shiny);
      } else {
        setNoShinyText("This pokemon has no shiny version :/");
      }
    }
  };

  return (
    <View style={styles.container}>
      {loading ? (
        <ActivityIndicator size="large" color="#0000ff" />
      ) : (
        pokemon && (
          <View>
            <PokemonImage shiny={shiny} sprites={pokemon.sprites} onPress={handlePress} />
            <PokemonInfo 
              name={pokemon.name}
              types={pokemon.types}
              height={pokemon.height}
              weight={pokemon.weight}
              baseExperience={pokemon.base_experience}
              abilities={pokemon.abilities}
            />
            <NoShinyText noShinyText={noShinyText} />
          </View>
        )
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
});
