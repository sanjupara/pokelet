import React from 'react';
import { Text, StyleSheet } from 'react-native';

const NoShinyText = ({ noShinyText }) => (
  noShinyText ? <Text style={styles.noShinyText}>{noShinyText}</Text> : null
);

const styles = StyleSheet.create({
  noShinyText: {
    fontSize: 18,
    color: 'red',
    marginTop: 20,
    textAlign: 'center',
  },
});

export default NoShinyText;
