import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const PokemonInfo = ({ name, types }) => (
  <View>
    <Text style={styles.name}>{name}</Text>
    <View style={styles.typesContainer}>
      {types.map((type, index) => (
        <Text key={index} style={styles.type}>{type.type.name}</Text>
      ))}
    </View>
  </View>
);

const styles = StyleSheet.create({
  name: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 20,
  },
  typesContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 10,
  },
  type: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 5,
    backgroundColor: '#f0f0f0',
    marginRight: 5,
  },
  detailsContainer: {
    marginTop: 10,
    alignItems: 'center',
  },
  details: {
    fontSize: 16,
    marginVertical: 2,
  },
});

export default PokemonInfo;
