import React from 'react';
import { Image, StyleSheet, TouchableOpacity } from 'react-native';

const PokemonImage = ({ shiny, sprites, onPress }) => (
  <TouchableOpacity onPress={onPress} style={styles.pokemonContainer}>
    <Image
      source={{ uri: shiny ? sprites.front_shiny : sprites.front_default }}
      style={styles.image}
    />
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  pokemonContainer: {
    alignItems: 'center',
  },
  image: {
    width: 200,
    height: 200,
  },
});

export default PokemonImage;
