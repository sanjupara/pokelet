export default class Pokemon {
    constructor(data) {
      this.id = data.id;
      this.name = data.name;
      this.height = data.height;
      this.weight = data.weight;
      this.baseExperience = data.base_experience;
      this.sprites = data.sprites;
      this.types = data.types;
      this.abilities = data.abilities;
    }
  }
  