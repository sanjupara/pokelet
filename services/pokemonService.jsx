import axios from 'axios';
import Pokemon from '../models/Pokemon';

const API_BASE_URL = 'https://pokeapi.co/api/v2/pokemon/';
const CRY_BASE_URL = 'https://veekun.com/dex/media/pokemon/cries/';

export const getPokemon = async (id) => {
  try {
    const response = await axios.get(`${API_BASE_URL}${id}`);
    return new Pokemon(response.data);
  } catch (error) {
    throw new Error('Fehler beim Laden des Pokémon.');
  }
};

export const getPokemonCryUrl = (id) => {
  return `${CRY_BASE_URL}${id}.ogg`;
};
